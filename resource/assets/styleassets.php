
<!-- Search in Drop down -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />

<!-- Bootstrap core CSS -->
<link href="../resource/css/style.css" rel="stylesheet" type="text/css">
<link href=".../resource/bootstrap.min.css" rel="stylesheet" media="screen">

<!-- Bootstrap Dropdown Hover CSS -->
<link href="../resource/css/animate.min.css" rel="stylesheet">
<link href="../resource/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

<!-- Include Date Range Picker -->
<script type="text/javascript" src="../resource/bootstrap/js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href=".../resource/bootstrap/css/daterangepicker.css" />

<!-- Required for Datepicker ended -->
<script src="../js/index.js"></script>

<!-- Select Search -->
<link href='../resource/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
<!-- Select Search ended-->