$(document).ready(function(){
    /* ########################################*/
    $("#branchid").change(function() {
        var branch = $("#branchid").val();
        if (branch !== 'SELECT' ) {
            $("#book").html('<td>Book</td><td>:</td><td><select  name="bookname" id="bookname" class="form-control text-uppercase" required><option> Select Book</option></option></select></td>');
            var postData='book';
            $.getJSON("list-data.php",{data:postData}, function(return_data){
                $.each(return_data.data, function(key,value){
                    $("#bookname").append("<option value="+value.value +">"+value.name+"</option>");
                });
            });

            $("#bookname").change(function(){
                var book=$("#bookname").val();
                postData=book;
                switch (book) {
                    case "CASH":
                        $("#searchform").attr('action', 'statement.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                    case "LEDGER":
                        $("#searchform").attr('action', 'ledger.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                    case "SALE":
                        $("#searchform").attr('action', 'allsalesledger.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                    case "BANK":
                        $("#searchform").attr('action', 'bankledger.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                    case "PARTY":
                        $("#searchform").attr('action', 'partyledger.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                    case "TBALANCE":
                        $("#searchform").attr('action', 'trialBalance.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                        case "STUDENT":
                        $("#searchform").attr('action', 'view.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;
                        case "ALLSTUDENT":
                        $("#searchform").attr('action', 'view.php');
                        //alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");
                        break;

                    default:
                        $("#searchform").attr('action', '#');
                }

                if(book=='LEDGER'){
                    $("#head").html('<td>Head Name</td><td>:</td><td><select id="accheadId" name="accheadId" class="form-control text-uppercase "></select></td>');
                    $.getJSON("list-data.php",{data:postData}, function(return_data){
                        $.each(return_data.data, function(key,value){
                            $("#accheadId").append("<option value="+value.id +">"+value.headnameenglish+"</option>");
                        });
                    });

                }
                if(book=='SALE' ){$("#head").html('<td>Head Name</td><td>:</td><td><select id="accheadId" name="accheadId"     class="form-control text-uppercase "><option class=\'\' value=\'\'></option>SELECT ONE<option class=\'\' value=\'24\'>STUDENT  (24)</option></select></td>');}
                if(book=='BANK' ){
                    $("#head").html('<td>Bank</td><td>:</td><td><select id="bankid" name="bankid"     class="form-control text-uppercase "></select></td>');
                    $.getJSON("list-data.php",{data:postData}, function(return_data){

                        $.each(return_data.data, function(key,value){
                            $("#bankid").append(
                                "<option value=" + value.id +">"+value.accountname+"</option>"
                            );
                        });
                    });

                }

                if(book=='PARTY' ){$("#head").html('<td>Party</td><td>:</td><td><select id="partyid" name="partyid"  class="form-control text-uppercase "></select></td>');
                    $.getJSON("list-data.php",{data:postData}, function(return_data){

                        $.each(return_data.data, function(key,value){
                            $("#partyid").append(
                                "<option value=" + value.id +">"+value.partyname+"</option>"
                            );
                        });
                    });

                }
                if(book=='STUDENT' ){
                    $("#class").html('<td>CLASS</td><td>:</td><td><select id="classid" name="classid"  class="form-control text-uppercase "><option>Select Class</option></select></td>');
                    var postDataStudent='STUDENT'
                    $.getJSON("list-data.php",{data:postDataStudent}, function(return_data){

                        $.each(return_data.data, function(key,value){
                            $("#classid").append(
                                "<option value=" + value.id +">"+value.name+"</option>"
                            );
                        });
                    });

                    $("#classid").change(function() {
                        var classid ='batchid';

                        $("#batch").html('<td>BATCH</td><td>:</td><td><select id="batchid" name="batchid"  class="form-control text-uppercase "><option>Select Batch(Year)</option></select></td>');
                        $.getJSON("list-data.php",{data:classid}, function(return_data){
                            $.each(return_data.data, function(key,value){
                                $("#batchid").append(
                                    "<option value=" + value.id +">"+value.year+"</option>"
                                );
                            });
                        });

                    });
                }
                //else{ $("#head").html(''); }
            });
        }else {$("#book").html(''); }
    });
    /*########################*/
    $("#head").change(function(){
        var headid=$("#accheadId").val();
        var book=$("#bookname").val();

        if(headid==24 && book!='LEDGER'){
            var postData=headid;
            $("#sale").html('<td>Product</td><td>:</td><td><select id=\'salename\'  name=\'salename\'  class=\'form-control text-uppercase\'><option value=\'all\'>ALL</option></select></td>');
            $.getJSON("list-data.php",{data:postData}, function(return_data){

                $.each(return_data.data, function(key,value){
                    $("#salename").append(
                        "<option value=" + value.id +">"+value.name+" ("+value.mobile+")"+"</option>"
                    );
                });
            });

        }

        else{  $("#sale").html('');   }
    });

});