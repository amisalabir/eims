<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\MainController\MainController();
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$allData = $objTransaction->statement();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();

$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $objBookTitle->search($_REQUEST);
$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);
if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;

$_SESSION['Page']= $page;
if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;
$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);
$serial = (  ($page-1) * $itemsPerPage ) +1;
if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBookTitle->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################
include_once ('header.php')?>
<!-- required for search, block 4 of 5 start -->
<div class="container form-group" align="right" >
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-5"> </div>
        <div class="col-sm-5">
            <form id="searchForm" action="statement.php"  method="get" style="margin-top: 5px; margin-bottom: 10px ">
                <input class="form-control" type="text" value="" id="searchID" name="search" placeholder="Search" width="60" > <br>
                <input type="checkbox"  name="byTitle"   checked  >By Particular
                <input type="checkbox"  name="byAuthor"  checked >By Invoice No
                <input type="checkbox"  name="id"   >By Trx ID
                <input type="checkbox"  name="advanced"   >Advanced
                <input hidden type="submit" class="btn-primary" value="search">
            </form>
        </div>
        <div class="col-sm-1"></div>
     </div>

</div>
<!-- required for search, block 4 of 5 end -->
<div class="container text-center "  >
    <h4> <?php if($page=='/transaction.php' || $page=='/inndividualTransaction.php'){echo "View transaction by date:";}  else{echo "View Cash Statement By Date:";}  ?>
    </h4>
    <form name="viewStatement" action="<?php echo "statement.php"; /*
    if($page=='/transaction.php' || $page=='/inndividualTransaction.php'){echo "inndividualTransaction.php";
    }
    elseif($page=='/orders.php' || $page=='/allorders.php'){echo "allorders.php";}
    else{echo "statement.php";} */?>
" method="get" id="">
    <div class="row">
    <div class="col-sm-4"> </div>
        <div class="col-sm-2">
   From: <input type="text" name="fromTransaction"  required id="from-datepicker" class="form-control from-datepicker" />
   </div>
    <div class="col-sm-2">
        TO :<input type="text" name="toTransaction" id="to-datepicker" required class="form-control to-datepicker"/>
    </div>
    <div class="col-sm-3">
        Branch :
        <select  name="branchid" id="branchid" class="form-control" required>
            <?php
            if($singleUser->role=='admin'){
                echo  "<option value='all'>All Branch</option>
                   <option value='1'>Head Office</option>
                   <option value='2'>Yard</option>";
            }else{echo  "<option value='2'>Yard</option>";}

            ?>
         </select>
        <br>

        <button type="submit" class="form-control btn btn-primary ">View</button>
    </div>
    <div class="col-sm-1"> </div>
     </div>
   </form>
</div>
<br>
<?php

include('footer_script.php');
?>
