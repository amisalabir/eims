<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\MainController\MainController();
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$objClasses=new \App\ExpenseIncome\Classes();
$allData = $objBookTitle->index();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();
$accountHead=$objTransaction->accounthead();
$bankNme=$objTransaction->allbank();
$class=$objClasses->classes();
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);


include_once ('header.php');
?>
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">
        function ChequeField(val){
            var element=document.getElementById('activeChequeField');
            if(val=='CASH CHEQUE'){
                element.style.display='block';
            }
            else{
                element.style.display='none';
            }
        }
    </script>
    <form class="form-group" name="vesselEntry" action="store.php" method="post">
        <input hidden name="addStudent" type="text" value="addStudent">
        <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">STUDENT NAME :</label> </div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control " id="" name="studentName" required type="text">
                    </div>
                        <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">FATHER'S NAME :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="fatherName"  type="text">
                        </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> MOTHER'S NAME :</label></div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control" name="motherName"  id="wastage"  type="text">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">CLASS :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="class"  class="form-control text-uppercase ">
                            <?php
                            foreach ($class as $singleClass){
                                echo  "<option class='text-uppercase' value='$singleClass->id'> $singleClass->classname  </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

              <div class="row">
                  <div class="col-sm-4 text-right form-group"><label for="wastageValue"> SESSION  :</label></div>
                  <div class="col-sm-3 text-left">
                      <input class="form-control" name="session"  id="wastageValue" required type="text">
                  </div>
                  <div class="col-sm-5"></div>
              </div>

                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcNo"> BATCH :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control" name="batch" type="text" required>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcDate">  DATE OF BIRTH :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control selectDate" name="birthDate"   type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="ldt">MOBILE :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control " id="" name="mobile" required type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarPrice">NID/BC NO :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="nid" type="text" >
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarRate">EMAIL :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="email"  type="email" >
                    </div>
                    <div class="col-sm-5"></div>
                </div>

                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> REMARKS :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
