<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}

################################ End of Session time calculation ##############################
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction = new \App\ExpenseIncome\Transaction();

$msg = Message::getMessage();

//var_dump($_POST); die();

if ($singleUser->role!='admin') {


    Message::message("You are not allowed.");
    Utility::redirect($_SERVER['HTTP_REFERER']);
}


if(isset($_POST['mark'])) {

$objBookTitle->trashMultiple($_POST['mark']);
    //Utility::redirect("trashed.php?Page=1");
    Utility::redirect($_SERVER['HTTP_REFERER']);

}
else
{
    Message::message("Empty Selection! Please select some records.");
    //Utility::redirect("index.php");
    Utility::redirect($_SERVER['HTTP_REFERER']);
}