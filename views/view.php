<?php 
include_once ('header.php');
include_once('printscript.php');
?>
<div align="center" class="content">
    <div align="center" class="container ctn">
        <div align="center" class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <form action="trashmultiple.php" method="post" id="multiple">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" name='submit' value='".$_GET['bookname']."'  class=\"btn btn-warning\">Trash Selected</button>
                            <a href='view.php?trashlist=".$_GET['bookname']."'   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
             <span><br><br> </span>
            <div class="row">
                <div class=" col-md-3 col-md-offset-8 text-right">
                    <input id="myInput" type="text" placeholder="Search..." >
                </div>
                <div class="col-md-1"></div>
            </div>
            <div id="dvContainer" class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 text-center" id="innerTable">
                    <style><?php include ('../resource/css/printsetup.css');   ?>   </style>
                    <?php
                    $printDate=date('Y-m-d');
$dataHead=<<<DATAHEAD
                        <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">KAZI SALA UDDIN.</font> <br>
                        <font style="font-size:14px">Colonnelhat, Akbarshah, Chittagong.</font><br>
                        <font style="font-size:13px">(Statement Since : $fromTransaction to $toTransaction)</font><br>

                        <div><b> $branch</b> </div> <div >Print Date:$printDate <div>
DATAHEAD;


                    if($_GET['bookname']=='STUDENT' ||$_GET['bookname']=='ALLSTUDENT' || $_GET['byStudent']=='on' || $_GET['trashlist']=='ALLSTUDENT'){
                        //
                        $objStudent = new \App\ExpenseIncome\Student();
                    if(isset($_POST)){
                        $objStudent->setData($_POST);
                    }else{$objStudent->setData($_POST);}
                        $allStudent=$objStudent->index();

                        echo $dataHead;
                        ?>
                    <table width="auto"   class="" >
                                                <thead>
                                                <tr style="background-color:#F2F2F2;">
                                                    <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                                    <th class="text-center">SL</th>
                                                    <th class="text-center" width="200px"> Name</th>
                                                    <th class="text-center" > Class </th>
                                                    <th class="text-center" > Batch </th>
                                                    <th class="text-center" > Mobile </th>
                                                    <th class="text-center" > Admission Date </th>
                                                    <th class="text-center">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                <?php
                                                foreach($allStudent as $oneData){
                                                    if($serial%2) $bgColor = "AZURE";
                                                    else $bgColor = "#ffffff";
                                                    echo "
                  <tr  style='background-color:'>
                     <td ><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>".$serial."</td>
                     <td class='text-left'> &nbsp; <a href='profile.php?id=$oneData->id'>$oneData->name</a> </td>
                     <td class='text-center'> $oneData->classname </td>
                     <td class='text-center'> $oneData->year </td>
                     <td class='text-center'> $oneData->mobile </td>
                     <td class='text-center'> $oneData->admissiondate </td>
                     <td class='text-right'>";
                                                    if($singleUser->role=='admin') {
                                                        echo "
                       <a role='menuitem' tabindex=-1' href='edit.php?accheadId=$oneData->id'>Edit</a> |
                       <a role='menuitem' tabindex=-1' href='delete.php?accheadId=$oneData->id'>Delete</a>";
                                                    }

                                                    echo"</td></tr>";
                                                    $serial++; }
                                                ?>
                                                </tbody>
                                            </table>

                <br>
                    <?php  }
                    ################################## View all class #############################

                    if($_GET['bookname']=='allClass')
                    {
                        $objClasses = new \App\ExpenseIncome\Classes();
                        $allClasses=$objClasses->classes();
                        echo $dataHead;
                     ?>
                        <style>
                            #viewclass td{
                                text-align: ;
                            }
                        </style>
                        <table width="auto" id="viewclass"   class="" >
                            <thead>
                            <tr style="background-color:#F2F2F2;">
                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                <th class="text-center">SL</th>
                                <th class="text-center" width=""> Name</th>
                                <th class="text-center" > Actions </th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                            <?php
                            foreach($allClasses as $oneClass){
                                if($serial%2) $bgColor = "AZURE";
                                else $bgColor = "#ffffff";
                                echo "
                  <tr  style='background-color:'>
                     <td ><input type='checkbox' class='checkbox' name='mark[]' value='$oneClass->id'></td>
                     <td style='text-align: center;'>".$serial."</td>
                     <td class='text-left'> &nbsp; $oneClass->name </td>
                     <td class='text-right'>";
                                if($singleUser->role=='admin') {
                                    echo "
                       <a role='menuitem' tabindex=-1' href='edit.php?classId=$oneClass->id'>Edit</a> |
                       <a role='menuitem' tabindex=-1' href='delete.php?classId=$oneClass->id'>Delete</a>";
                                }

                                echo"</td></tr>";
                                $serial++; }
                            ?>
                            </tbody>
                        </table>

                    <?php
                    }
                    ################################## View all Head #############################

                    if($_GET['id']=='viewhead')
                    {
                        $objHeads = new \App\ExpenseIncome\Head();
                        $allHead=$objHeads->heads();
                        echo $dataHead;
                     ?>
                        <style>
                            #viewclass td{
                                text-align: ;
                            }
                        </style>
                        <table width="auto" id="viewclass"   class="" >
                            <thead>
                            <tr style="background-color:#F2F2F2;">
                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                <th class="text-center">SL</th>
                                <th class="text-center" width="">Head Name</th>
                                <th class="text-center" > Actions </th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                            <?php
                            foreach($allHead as $oneClass){
                                if($serial%2) $bgColor = "AZURE";
                                else $bgColor = "#ffffff";
                                echo "
                  <tr  style='background-color:'>
                     <td ><input type='checkbox' class='checkbox' name='mark[]' value='$oneClass->id'></td>
                     <td style='text-align: center;'>".$serial."</td>
                     <td class='text-left'> &nbsp;  <a role='menuitem' tabindex=-1' href='edit.php?headid=$oneClass->id'>$oneClass->headnameenglish</a> </td>
                     <td class='text-right'>";
                                if($singleUser->role=='admin') {
                                    echo "
                       <a role='menuitem' tabindex=-1' href='edit.php?classId=$oneClass->id'>Edit</a> |
                       <a role='menuitem' tabindex=-1' href='delete.php?classId=$oneClass->id'>Delete</a>";
                                }

                                echo"</td></tr>";
                                $serial++; }
                            ?>
                            </tbody>
                        </table>

                    <?php
                    }
                    ################################## View all Batch #############################

                    if($_GET['bookname']=='allBatch'){
                        $objBatch = new \App\ExpenseIncome\Batch();
                        $allBatch= $objBatch->batches();
                        echo $dataHead;
                        ?>
                        <style>
                            #viewclass td{
                                text-align: ;
                            }
                        </style>
                        <table width="auto" id="viewclass"   class="" >
                            <thead>
                            <tr style="background-color:#F2F2F2;">
                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                <th class="text-center">SL</th>
                                <th class="text-center" width="">Batch</th>
                                <th class="text-center" > Actions </th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                            <?php
                            foreach($allBatch as $oneBatch){
                                if($serial%2) $bgColor = "AZURE";
                                else $bgColor = "#ffffff";
                                echo "
                  <tr  style='background-color:'>
                     <td ><input type='checkbox' class='checkbox' name='mark[]' value='$oneBatch->id'></td>
                     <td style='text-align: center;'>".$serial."</td>
                     <td class='text-left'> &nbsp;  <a role='menuitem' tabindex=-1' href='edit.php?batchId=$oneBatch->id'>$oneBatch->year</a> </td>
                     <td class='text-right'>";
                                if($singleUser->role=='admin') {
                                    echo "
                       <a role='menuitem' tabindex=-1' href='edit.php?batchId=$oneBatch->id'>Edit</a> |
                       <a role='menuitem' tabindex=-1' href='delete.php?batchId=$oneBatch->id'>Delete</a>";
                                }

                                echo"</td></tr>";
                                $serial++; }
                            ?>
                            </tbody>
                        </table>

                        <?php
                    }
                    ?>

                </div>
                <div class="col-sm-1"></div>

            </div>

        </form>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
echo "Update";
?>
