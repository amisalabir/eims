<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction = new \App\ExpenseIncome\Transaction();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

$allData =$objTransaction->setData($_GET);
//$transactionData = $objTransaction->statement();
###################################### Previous & Next Data Calculation ##################################
//

$prevDate=date('Y-m-01');
$nextDate=date('Y-m-01');

if(isset($_GET['monthid'])){

if($_GET['monthid']=='prevtMonth'){

    $dt=$_GET['prevDate'];
    $date = strtotime(date("Y-m-d", strtotime($dt)) . ", first day of this month");
    $newdate = strtotime ( '-1 MONTH' , $date ) ;
    $newdate = date ( 'Y-m-d' , $newdate );

    $prev_last_date_find = strtotime(date("Y-m-d",strtotime($newdate)) . ", last day of this month");

    $prev_last_date = date('Y-m-d' ,$prev_last_date_find);
    $prevDate=$newdate;

    $_GET['fromTransaction']= $prevDate;
    $_GET['toTransaction']= $prev_last_date;

    $nextDate=$newdate;
    //echo "</br>".$newdate;
    //echo "</br>".$prev_last_date;

    $allData =$objTransaction->setData($_GET);
}
if($_GET['monthid']=='nextMonth'){

    $ndt=$_GET['nextDate'];

    $NewDate = strtotime(date("Y-m-d", strtotime($ndt)) . ", first day of this month");
    $nextDate = strtotime ( '+1 MONTH' , $NewDate ) ;
    $nextNewDate = date ( 'Y-m-d' , $nextDate);

    $next_last_date_find = strtotime(date("Y-m-d",strtotime($nextNewDate)) . ", last day of this month");
    $next_last_date = date('Y-m-d' ,$next_last_date_find);

    $nextDate= $nextNewDate;

    $_GET['fromTransaction']= $nextNewDate;
    $_GET['toTransaction']= $next_last_date;

    $prevDate=$nextNewDate;
    $nextDate=$nextNewDate;

    //echo "</br>".$nextNewDate;
   // echo "</br>".$next_last_date;

    $allData =$objTransaction->setData($_GET);
    }

    $fromTransaction=$_GET['fromTransaction'];
    $toTransaction=$_GET['toTransaction'];
    $branch="(Personal)";
}
//var_dump($_GET); die();
###################################### Previous & Next Data Calculation ##################################

if ($singleUser->role=='admin') {
    if(isset($_GET['bookname'])){
    if ($_GET['bookname'] == 'CASH' && ($_GET['branchid'] == '2' || $_GET['branchid'] == '3'|| $_GET['branchid'] == '4'|| $_GET['branchid'] == '5') )
        $transactionData = $objTransaction->statement();
    if ($_GET['bookname'] == 'CASH' && $_GET['branchid'] == '1')
        $transactionData = $objTransaction->bothstatement();
    }
    $transactionData = $objTransaction->statement();
}
else{
    if(isset($_GET['bookname'])) {
        if ($_GET['bookname'] == 'CASH' && ($_GET['branchid'] == '2' || $_GET['branchid'] == '3' || $_GET['branchid'] == '4' || $_GET['branchid'] == '5'))
            $transactionData = $objTransaction->statement();
    }
    $transactionData = $objTransaction->statement();
}
/*Branch selection*/
if(isset($_GET['bookname'])) {

    $fromTransaction = $_GET['fromTransaction'];
    $toTransaction = $_GET['toTransaction'];
}
if($_GET['branchid']=='1') $branch="(All Braches)";
if($_GET['branchid']=='2') $branch="(Head Office";
if($_GET['branchid']=='3') $branch="(Oline IT)";
if($_GET['branchid']=='4') $branch="(Petty Cash)";
if($_GET['branchid']=='5') $branch="(Personal)";
if(isset($_GET['currentMonth'])) {
    $branch="(Personal)";
      $fromTransaction=date('Y-m-01');
      $toTransaction=date('Y-m-t');
}


################## statement  block start ####################

$_SESSION['someData']=$transactionData;

//Converting Object to an Array
$objToArray = json_decode(json_encode($transactionData), True);

$serial = 1;
################## statement  block end ######################
include_once ('header.php');
include_once('printscript.php');
?>
<div align="center" class="content">
    <div align="center" class="container ctn">
        <div align="center" class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <form action="trashmultiple.php" method="post" id="multiple">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
            <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
            <span><br><br> </span>
            <div class="row">
                <div class=" col-md-3 col-md-offset-8 text-right">
                    <input id="myInput" type="text" placeholder="Search." >
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row"  id="dvContainer" align="center">
                   <style>

                           <?php
                            include ('../resource/css/printsetup.css')
                            ?>
                        </style>
                         <div class="col-sm-1"></div>
                         <div id="innerTable" class="col-sm-10 text-center" align="center" >

                             <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">KAZI SALA UDDIN.</font> <br>
                             <font style="font-size:14px">Colonnelhat, Akbarshah, Chittagong.</font><br>
                             <font style="font-size:13px">(<?php echo "Statement Since : ".$fromTransaction." to ".$toTransaction;?>)</font><br>

                             <div><b> <?php  echo "HEAD: Cash Statement ".$branch; ?></b> </div> <div ><?php echo "Print Date: ";  echo date('Y-m-d'); ?><div>


                             <table width="auto"   >
                                            <thead>
                                            <tr class="class="header"" style="background-color:#F2F2F2;">
                                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                                <th class="text-center">SL</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center" width="500px">Description</th>
                                                <th class="text-center">Voucher/ <br>Challan No</th>
                                                <th class="text-center">Received <br> (Taka)</th>
                                                <th class="text-center">Payment <br> (Taka)</th>
                                                <th class="text-center">Balance</th>
                                            </tr>
                                            </thead>
                                            <tbody id="myTable">
                                            <?php
                                            $totalAmountIn=0;
                                            $totalAmountOut=0;
                                            $balance=0;
                                            //  $serial= 1;  ##### We need to remove this line to work pagination correctly.
                                            // echo "<pre>"; var_dump($allData); echo "</pre>"; die();
                                            foreach($transactionData as $oneData){ ########### Traversing $someData is Required for pagination  #############
                                                //$totalDays=abs('$oneData->toDurationDate','$oneData->fromDurationDate');
                                                if($serial%2) $bgColor = "AZURE";
                                                else $bgColor = "#ffffff";
                                                $totalAmountIn=$totalAmountIn+$oneData->amountIn;
                                                $totalAmountOut=$totalAmountOut+$oneData->amountOut;
                                                //$totalAmount=$totalAmount+$oneData->amount ;
                                                $balance=($balance-$oneData->amountOut)+$oneData->amountIn;
                                                $voucherType=""; $voucherOrChallan="";
                                                if($oneData->voucherNo!=Null){$voucherType="Dr -"; $voucherOrChallan=$oneData->voucherNo;}else{$voucherType="Cr -"; $voucherOrChallan=$oneData->crvoucher;}
                                                if($oneData->challanno!=Null ||($oneData->voucherNo==Null && $oneData->crvoucher==Null )){$voucherType="Ch -"; $voucherOrChallan=$oneData->challanno;}
                                                echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='padding-left:5px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>";
                                                if($singleUser->role=='admin'){ echo "<a role='menuitem' tabindex=-1' href='edit.php?id=$oneData->id'>$serial</a>";} else{echo $serial;}
                                                echo"</td>
                      <td class='text-center'>$oneData->transactionDate</td>
                     <td class='text-left'> $oneData->headnameenglish: $oneData->accountname $oneData->studentname <br> $oneData->transactionFor $oneData->remarks  </td>
                     <td class='text-center'>$voucherType $voucherOrChallan</td>
                     <td style='text-align: right;'>".number_format($oneData->amountIn,0)."</td>
                     <td style='text-align: right;'>".number_format($oneData->amountOut,0)."</td>
                     <td class='text-right'>".number_format($balance,0)."</td>
                  </tr>
              ";
                                                $serial++; }
                                            echo "     
                        <tr style='background-color:; font-weight: bold;'>
                            <td style='text-align: right;' colspan='5'> Total Taka: <span>&nbsp;&nbsp; </span></td>
                            <td class='text-right'>".number_format($totalAmountIn,0)." </td>
                            <td class='text-right'>".number_format($totalAmountOut,0)."</td>
                            <td class='tablefooter text-right'>".number_format($balance,0)." </td>
                        </tr>
                        "; ?>
                            </tbody>
                      </table>
                         <div class="col-sm-1"></div>
                     </div>
                <div  class="row">
                <div class="col-sm-12 tex-center">
                    <a href="statement.php?monthid=prevtMonth&prevDate=<?php echo $prevDate; echo "&branchid=".$_GET['branchid']; ?>"><< Previous</a> |
                    <a href="statement.php?monthid=nextMonth&nextDate=<?php echo  $nextDate; echo "&branchid=".$_GET['branchid']; ?>">Next >></a>
                </div>
                </div>
        </form>
       <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
