<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="#">
    
    <title>::FORM::</title>

    <!-- Bootstrap core CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
	
    <!-- Custom styles for this template -->
    <link href="navbar-fixed-top.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
	
  </head>
  <body id="body">
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="top">
					<h3 class="text-center">LOG IN</h3>
				</div>
				<div class="login">
					<center><img class="img-circle" width="80px" src="img/icon/admin.png"/></center>
					<form>
						<input type="text" class="form-control" name="name" placeholder="User Name" required>
						<input type="password" class="form-control" name="name" placeholder="Password" required>
						<br/><input type="submit" class="btn btn-primary" name="submit" value="Sign in">
					</form>
					<p class="text-right">Powered By- Oline-IT</p>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
  
  <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
	<!--<script>window.jQuery || document.write('<script src="js/jquery.min.js"></script>')</script>-->
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<!-- Bootstrap Dropdown Hover JS -->
    <script src="js/bootstrap-dropdownhover.min.js"></script>
  </body>
  </html>