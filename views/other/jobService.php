<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Job Receive</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Receive Date</td>
								<td>:</td>
								<td><input type="recvdate" class="form-control" name="date" required></td>
							</tr>
							<tr>
								<td>Voucher No</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="voucherNo" required></td>
							</tr>
							<tr>
								<td>Receive Type</td>
								<td>:</td>
								<td>
									<select name="recvType" class="form-control" required>
									  <option value="Advance" selected>Advance</option>
									  <option value="Normal">Normal</option>
									  <option value="Discount">Discount</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Company ID</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="comId" required></td>
							</tr>
							<tr>
								<td>Job No & Year</td>
								<td>:</td>
								<td>
								<div class="col-auto form-inline">
									<input type="text" name="jobNo" class="form-control" required>
									<input style="width:50px;" type="text" name="jobNo2" class="form-control" required>
									<input style="width:150px;" type="text" name="jobYear" placeholder="Job Year" class="form-control" required>
								</div>
								</td>
							</tr>
							<tr>
								<td>Party ID</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="partyId" required></td>
							</tr>
							<tr>
								<td>Cash/Bank ID</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="cashbankId" required></td>
							</tr>
							<tr>
								<td>Remarks</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="remarks" required></td>
							</tr>
							<tr>
								<td>Amount</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="amount" required></td>
							</tr>
							<tr>
								<td>In Words</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="inWords" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>
									<div class="col-auto form-inline">
										<input style="margin-right:60px;" type="submit" class="btn btn-primary" name="submit" value="Save">
										<input type="submit" class="btn btn-primary" name="print" value="Print">
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 