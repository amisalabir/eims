<?php
################## search  block 1 of 5 start ##################
$objSearch=new \App\ExpenseIncome\Search();
if(isset($_REQUEST['search']) )$someData =  $objSearch->search($_REQUEST);
$availableKeywords=$objSearch->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################


################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
	$someData = $objSearch->search($_REQUEST);
	$transactionData=$someData;
	$serial = 1;
}
################## search  block 2 of 5 end ##################

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="#">
     <title>::EIMS::</title>
	  <!-- required for search, block3 of 5 start -->
	  <link rel="stylesheet" href="../resource/bootstrap/css/jquery-ui.css">
	  <script src="../resource/bootstrap/js/jquery.js"></script>
	  <script src="../resource/bootstrap/js/jquery-ui.js"></script>

	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

	  <!--   required for search, block3 of 5 end -->

      <!-- Search in Drop down -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
      <!-- Bootstrap core CSS -->
      <link href="../resource/css/style.css" rel="stylesheet" type="text/css">
      <link href="../resource/css/bootstrap.min.css" rel="stylesheet" media="screen">
      <!-- Bootstrap Dropdown Hover CSS -->
      <link href="../resource/css/animate.min.css" rel="stylesheet">
      <link href="../resource/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
     <!-- Include Date Range Picker -->
      <script type="text/javascript" src="../resource/bootstrap/js/daterangepicker.js"></script>
      <link rel="stylesheet" type="text/css" href="../resource/bootstrap/css/daterangepicker.css" />

      <!-- Required for Datepicker ended -->
      <script src="../resource/js/index.js"></script>
      <!-- Select Search -->
      <link href='../resource/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>

      <!-- Select Search ended
      <link href='../resource/clock/css/style.css' rel='stylesheet' type='text/css' media="screen">
      -->

  </head>
  <body id="body">
	<div class="container headSection">
		<div class="row">
				<div class="row">
					<div class="col-md-9">
                        <a href="index.php" class="tittle"><h3>Expense & Income Management System.</h3></a>
					</div>
					<div class="col-md-3 text-right tittle">
						<p>Hi ! <b><?php echo "<b>$singleUser->first_name $singleUser->last_name</b>"?>!</b> &nbsp; <a class="tittle" href="User/Authentication/logout.php">[ Log Out ]</a></p>
                        <p id='demo4' class="text-center cal-container"></p>
					</div>
				</div>
			<div class="row">
				<div class="col-md-offset-9 col-md-3 text-right searchbox">
					<!-- required for search, block 4 of 5 start -->
					<form class="form-group" id="searchForm" action="statement.php"  method="get" style="margin-top: 5px;  ">
						<input class="form-control" type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
						<input  type="checkbox" id="searchStudent"  name="byStudent" >Student
						<input  type="checkbox"  name="byRemarks" checked  >By Remarks
						<input  type="checkbox"  name="byHead"  >By Head
						<input hidden type="submit" class="btn-primary" value="search">
					</form>
					<!-- required for search, block 4 of 5 end -->
				</div>
			</div>
				<nav  class="navbar navbar-default navbar-static">
						<div style="padding-left:0px; padding-right:0px;" class="container">
							<div style="padding-top:0px; margin:0px;" class="">
							</div>
							<div style="padding-top:0px; margin-top:0px;" class="navbar-header">
							  <button  type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>                        
							  </button>
							  <!--<a class="navbar-brand" href="#myPage"><h4>BSBL</h4></a>-->
							</div>
							<div style="padding-top:0px; margin-top:0px;"  class="collapse navbar-collapse" id="myNavbar">
								<div style="padding-left:0px; padding-right:0px;" class="container">
								  <ul class="nav navbar-nav navbar-left">
									<li class="backg"><a href="index.php">HOME</a></li>
									<li class="dropdown">
										<a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">ACCOUNT INPUTS<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li class="backg"><a id="menu" href="addTransaction.php">Single Transaction Entry</a></li>
											<li class="backg"><a id="menu" href="other/multipleVoucher.php">Multiple Voucher Entry</a></li>
											<li class="divider"></li>
                                            <li class="backg"><a id="menu" href="create.php?id=addStudent">Add Student</a></li>
                                            <li class="backg"><a id="menu" href="view.php?bookname=ALLSTUDENT">View Students</a></li>
											<li class="divider"></li>
											<li class="backg"><a id="menu" href="create.php?id=addClass">Add Class</a></li>
											<li class="backg"><a id="menu" href="view.php?bookname=allClass">View Class</a></li>
											<li class="divider"></li>
											<li class="backg"><a id="menu" href="create.php?id=addBatch">Add Batch</a></li>
											<li class="backg"><a id="menu" href="view.php?bookname=allBatch">View Batch</a></li>
											<li class="divider"></li>
                                            <li class="backg"><a id="menu" href="create.php?id=addShead">Add Account Head</a></li>
                                            <li class="backg"><a id="menu" href="view.php?id=viewhead">View Head</a></li>
                                            <li class="backg"><a id="menu" href="create.php?id=addBank">Add Bank</a></li>
                                            <li class="backg"><a id="menu" href="addParty.php">Add Party</a></li>

										</ul>
									</li>

									<li class="dropdown">
										<a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">ACOUNT REPORTS<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li class="backg"><a id="menu" href="statement.php?branchid=5&currentMonth=currentMonth&currentDate=<?php echo date('Y-m-01'); ?>">Cash Book</a></li>
											<li class="backg"><a id="menu" href="#">Bank Book</a></li>
											<li class="backg"><a id="menu" href="#">Ledger Book</a></li>
											<li class="backg"><a id="menu" href="#">Trial Balance</a></li>
											<li class="backg"><a id="menu" href="#">Trading Account</a></li>
											<li class="backg"><a id="menu" href="#">P/L Account</a></li>
											<li class="backg"><a id="menu" href="#">Statement of Affairs</a></li>
											<li class="backg"><a id="menu" href="opneningbalance.php">Opnening Balance</a></li>
											<li class="divider"></li>

										</ul>
									</li>
                                    <?php
                                    if ($singleUser->role=='admin'){
                                        echo "<li class=\"dropdown\">
										<a id=\"menu\" href=\"\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" data-hover=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" data-animations=\"zoomIn zoomIn zoomIn zoomIn\">SETTINGS <span class=\"caret\"></span></a>
										<ul class=\"dropdown-menu\">
											<li class=\"backg\"><a id=\"menu\" href=\"create.php?id=user\">Create User</a></li>
											<li class=\"backg\"><a id=\"menu\" href=\"#\">Import Data</a></li>
										</ul>
									</li>";
                                    }
                                    ?>

								  </ul>
								</div>
							</div>
						</div>
				</nav>
		</div>
	</div>
