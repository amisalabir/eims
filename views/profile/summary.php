<?php

if(!isset($_SESSION) ) session_start();
?>


<div id="clientsummarycontainer">

    <div class="clientsummaryactions">
        Exempt from Tax: <span id="taxstatus" class="csajaxtoggle" style="text-decoration:underline;cursor:pointer"><strong class="textred">No</strong></span>
        &nbsp;&nbsp;
        Auto CC Processing: <span id="autocc" class="csajaxtoggle" style="text-decoration:underline;cursor:pointer"><strong class="textgreen">Yes</strong></span>
        &nbsp;&nbsp;
        Send Overdue Reminders: <span id="overduenotices" class="csajaxtoggle" style="text-decoration:underline;cursor:pointer"><strong class="textgreen">Yes</strong></span>
        &nbsp;&nbsp;
        Apply Late Fees: <span id="latefees" class="csajaxtoggle" style="text-decoration:underline;cursor:pointer"><strong class="textgreen">Yes</strong></span>
    </div>
    <div id="userdetails" style="font-size:18px;">#<span id="userId">58</span> - Adel Mamun</div>



    <table width="100%">
        <tbody><tr><td width="25%" valign="top">

                <div class="clientssummarybox">
                    <div class="title">Clients Information</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr><td width="110">First Name</td><td>Adel</td></tr>
                        <tr class="altrow"><td>Last Name</td><td>Mamun</td></tr>
                        <tr><td>Company Name</td><td>Orange Communication</td></tr>
                        <tr class="altrow"><td>Email Address</td><td>azizuddin2007@gmail.com</td></tr>
                        <tr><td>Address 1</td><td>72/A Haji T Ali Ln</td></tr>
                        <tr class="altrow"><td>Address 2</td><td></td></tr>
                        <tr><td>City</td><td> Chittagong</td></tr>
                        <tr class="altrow"><td>State/Region</td><td>N/A</td></tr>
                        <tr><td>Postcode</td><td>4000</td></tr>
                        <tr class="altrow"><td>Country</td><td>BD - Bangladesh</td></tr>
                        <tr><td>Phone Number</td><td>01772579956</td></tr>
                        </tbody></table>
                    <ul>
                        <li><a id="summary-reset-password" href="clientssummary.php?userid=58&amp;resetpw=true&amp;token=cab00baa0b5f23abac96b5877788d3c3b008cd4d"><img src="images/icons/resetpw.png" border="0" align="absmiddle"> Reset &amp; Send Password</a>
                        </li><li><a id="summary-cccard-details" href="#" onclick="openCCDetails();return false"><img src="images/icons/offlinecc.png" border="0" align="absmiddle"> Credit Card Information</a>
                        </li><li><a id="summary-login-as-client" href="../dologin.php?username=azizuddin2007%40gmail.com"><img src="images/icons/clientlogin.png" border="0" align="absmiddle"> Login as Client</a>
                        </li></ul>
                </div>

                <div class="clientssummarybox">
                    <div class="title">Contacts/Sub-Accounts</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr><td align="center">No additional contacts setup</td></tr>
                        </tbody></table>
                    <ul>
                        <li><a href="clientscontacts.php?userid=58&amp;contactid=addnew"><img src="images/icons/clientsadd.png" border="0" align="absmiddle"> Add Contact</a>
                        </li></ul>
                </div>

            </td><td width="25%" valign="top">

                <div class="clientssummarybox">
                    <div class="title">Invoices/Billing</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr><td width="110">Paid</td><td>4 ( BDT. 21500.00)</td></tr>
                        <tr class="altrow"><td>Unpaid/Due</td><td>2 ( BDT. 15800.00)</td></tr>
                        <tr><td>Cancelled</td><td>0 ( BDT. 0.00)</td></tr>
                        <tr class="altrow"><td>Refunded</td><td>0 ( BDT. 0.00)</td></tr>
                        <tr><td>Collections</td><td>0 ( BDT. 0.00)</td></tr>
                        <tr class="altrow"><td><strong>Income</strong></td><td><strong> BDT. 24700.00</strong></td></tr>
                        <tr><td>Credit Balance</td><td> BDT. 0.00</td></tr>
                        </tbody></table>
                    <ul>
                        <li><a href="invoices.php?action=createinvoice&amp;userid=58&amp;token=cab00baa0b5f23abac96b5877788d3c3b008cd4d"><img src="images/icons/invoicesedit.png" border="0" align="absmiddle"> Create Invoice</a>
                        </li><li><a href="#" onclick="showDialog('addfunds');return false"><img src="images/icons/addfunds.png" border="0" align="absmiddle"> Create Add Funds Invoice</a>
                        </li><li><a href="#" onclick="showDialog('geninvoices');return false"><img src="images/icons/ticketspredefined.png" border="0" align="absmiddle"> Generate Due Invoices</a>
                        </li><li><a href="clientsbillableitems.php?userid=58&amp;action=manage"><img src="images/icons/billableitems.png" border="0" align="absmiddle"> Add Billable Item</a>
                        </li><li><a href="#" onclick="window.open('clientscredits.php?userid=58','','width=750,height=350,scrollbars=yes');return false"><img src="images/icons/income.png" border="0" align="absmiddle"> Manage Credits</a>
                        </li><li><a href="quotes.php?action=manage&amp;userid=58"><img src="images/icons/quotes.png" border="0" align="absmiddle"> Create New Quote</a>
                        </li></ul>
                </div>

                <div class="clientssummarybox">
                    <div class="title">Other Information</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr><td width="110">Status</td><td>Active</td></tr>
                        <tr class="altrow"><td>Client Group</td><td>Domain &amp; Hosting </td></tr>
                        <tr><td>Signup Date</td><td>09/11/2017</td></tr>
                        <tr class="altrow"><td>Client For</td><td>16 Months</td></tr>
                        <tr><td width="110">Last Login</td><td>No Login Logged</td></tr>
                        </tbody></table>
                </div>

            </td><td width="25%" valign="top">

                <div class="clientssummarybox">
                    <div class="title">Products/Services</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr><td width="140">Shared Hosting</td><td>3 (4 Total)</td></tr>
                        <tr class="altrow"><td>Reseller Hosting</td><td>0 (0 Total)</td></tr>
                        <tr><td>VPS/Server</td><td>0 (0 Total)</td></tr>
                        <tr class="altrow"><td>Product/Service</td><td>2 (2 Total)</td></tr>
                        <tr><td>Domains</td><td>1 (1 Total)</td></tr>
                        <tr class="altrow"><td>Accepted Quotes</td><td>0 (0 Total)</td></tr>
                        <tr><td>Support Tickets</td><td>0 (0 Total)</td></tr>
                        <tr class="altrow"><td>Affiliate Signups</td><td>0</td></tr>
                        </tbody></table>
                    <ul>
                        <li><a href="orders.php?clientid=58"><img src="images/icons/orders.png" border="0" align="absmiddle"> View Orders</a>
                        </li><li><a href="ordersadd.php?userid=58"><img src="images/icons/ordersadd.png" border="0" align="absmiddle"> Add New Order</a>
                        </li></ul>
                </div>

                <div class="clientssummarybox">
                    <div class="title">Files</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr><td align="center">No files uploaded</td></tr>
                        </tbody></table>
                    <ul>
                        <li><a href="#" id="addfile"><img src="images/icons/add.png" border="0" align="absmiddle"> Add File</a>
                        </li></ul>
                    <div id="addfileform" style="display:none;">
                        <img src="images/spacer.gif" width="1" height="4"><br>
                        <form method="post" action="clientssummary.php?userid=58&amp;action=uploadfile" enctype="multipart/form-data">
                            <input type="hidden" name="token" value="cab00baa0b5f23abac96b5877788d3c3b008cd4d">
                            <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                                <tbody><tr><td width="40">Title</td><td class="fieldarea"><input type="text" name="title" style="width:90%"></td></tr>
                                <tr><td>File</td><td class="fieldarea"><input type="file" name="uploadfile" style="width:90%"></td></tr>
                                <tr><td></td><td class="fieldarea"><input type="checkbox" name="adminonly" value="1"> Admin Only &nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" value="Submit"></td></tr>
                                </tbody></table>
                        </form>
                    </div>
                </div>

                <div class="clientssummarybox">
                    <div class="title">Recent Emails</div>
                    <table class="clientssummarystats" cellspacing="0" cellpadding="2">
                        <tbody><tr class=""><td align="center">05/02/2019 01:40 - <a href="#" onclick="window.open('clientsemails.php?&amp;displaymessage=true&amp;id=2035','','width=650,height=400,scrollbars=yes');return false">New Account Information</a></td></tr>
                        <tr class="altrow"><td align="center">05/02/2019 01:33 - <a href="#" onclick="window.open('clientsemails.php?&amp;displaymessage=true&amp;id=2034','','width=650,height=400,scrollbars=yes');return false">New Account Information</a></td></tr>
                        <tr class=""><td align="center">07/01/2019 10:37 - <a href="#" onclick="window.open('clientsemails.php?&amp;displaymessage=true&amp;id=2007','','width=650,height=400,scrollbars=yes');return false">Order Confirmation</a></td></tr>
                        <tr class="altrow"><td align="center">05/01/2019 11:40 - <a href="#" onclick="window.open('clientsemails.php?&amp;displaymessage=true&amp;id=2006','','width=650,height=400,scrollbars=yes');return false">Invoice Payment Confirmation</a></td></tr>
                        <tr class=""><td align="center">05/01/2019 11:39 - <a href="#" onclick="window.open('clientsemails.php?&amp;displaymessage=true&amp;id=2005','','width=650,height=400,scrollbars=yes');return false">Invoice Payment Confirmation</a></td></tr>
                        </tbody></table>
                </div>

            </td><td width="25%" valign="top">

                <div class="clientssummarybox">
                    <div class="title">Other Actions</div>
                    <ul>
                        <li><a href="addonmodules.php?module=project_management&amp;view=user&amp;userid=58"><img src="images/icons/invoices.png" border="0" align="absmiddle"> View Projects</a></li>
                        <li><a href="reports.php?report=client_statement&amp;userid=58"><img src="images/icons/reports.png" border="0" align="absmiddle"> View Account Statement</a>
                        </li><li><a href="supporttickets.php?action=open&amp;userid=58"><img src="images/icons/ticketsopen.png" border="0" align="absmiddle"> Open New Support Ticket</a>
                        </li><li><a href="supporttickets.php?view=any&amp;client=58"><img src="images/icons/ticketsother.png" border="0" align="absmiddle"> View all Support Tickets</a>
                        </li><li><a href="clientssummary.php?userid=58&amp;activateaffiliate=true&amp;token=cab00baa0b5f23abac96b5877788d3c3b008cd4d"><img src="images/icons/affiliates.png" border="0" align="absmiddle"> Activate as Affiliate</a>
                        </li><li><a href="#" onclick="window.open('clientsmerge.php?userid=58','movewindow','width=500,height=280,top=100,left=100,scrollbars=1');return false"><img src="images/icons/clients.png" border="0" align="absmiddle"> Merge Clients Accounts</a>
                        </li><li><a href="#" onclick="closeClient();return false" style="color:#000000;"><img src="images/icons/delete.png" border="0" align="absmiddle"> Close Clients Account</a>
                        </li><li><a href="#" onclick="deleteClient();return false" style="color:#CC0000;"><img src="images/icons/delete.png" border="0" align="absmiddle"> Delete Clients Account</a>
                        </li></ul>
                </div>

                <div class="clientssummarybox">
                    <div class="title">Send Email</div>
                    <form action="clientsemails.php?userid=58&amp;action=send&amp;type=general" method="post">
                        <input type="hidden" name="token" value="cab00baa0b5f23abac96b5877788d3c3b008cd4d">
                        <input type="hidden" name="id" value="58">
                        <div align="center"><select name="messagename"><option value="newmessage">New Message</option><option value="" style="background-color:#efefef"></option><option value="Automated Password Reset">Automated Password Reset</option><option value="Client Signup Email">Client Signup Email</option><option value="Credit Card Expiring Soon">Credit Card Expiring Soon</option><option value="Order Confirmation">Order Confirmation</option><option value="Password Reset Confirmation">Password Reset Confirmation</option><option value="Quote Accepted">Quote Accepted</option><option value="Quote Accepted Notification">Quote Accepted Notification</option><option value="Quote Delivery with PDF">Quote Delivery with PDF</option><option value="Unsubscribe Confirmation">Unsubscribe Confirmation</option></select> <input type="submit" value="Go" class="button"></div>
                    </form>
                    <center><a href="addonmodules.php?module=mim_sms&amp;tab=sendbulk&amp;cid=58">Send SMS Message</a></center>
                </div>

                <div class="clientssummarybox">
                    <div class="title">Admin Notes</div>
                    <form method="post" action="/billing/salabir/clientssummary.php?userid=58&amp;action=savenotes">
                        <input type="hidden" name="token" value="cab00baa0b5f23abac96b5877788d3c3b008cd4d">
                        <div align="center">
<tTest
                        </div>
                    </form>
                </div>

            </td></tr>
        <tr><td colspan="4">

                <p align="right"><input type="button" value="Status Filter: Off" class="btn-small" onclick="toggleStatusFilter()"></p>
                <div id="statusfilter">
                    <form>
                        <div class="checkall">
                            <label><input type="checkbox" id="statusfiltercheckall" onclick="checkAllStatusFilter()" checked=""> Check All</label>
                        </div>
                        <table class="datatable" width="100%" cellspacing="1" cellpadding="3" border="0">
                            <tbody><tr>
                                <th></th>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Pending" onclick="uncheckCheckAllStatusFilter()" checked=""> Pending</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Pending Transfer" onclick="uncheckCheckAllStatusFilter()" checked=""> Pending Transfer</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Active" onclick="uncheckCheckAllStatusFilter()" checked=""> Active</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Suspended" onclick="uncheckCheckAllStatusFilter()" checked=""> Suspended</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Terminated" onclick="uncheckCheckAllStatusFilter()" checked=""> Terminated</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Cancelled" onclick="uncheckCheckAllStatusFilter()" checked=""> Cancelled</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Expired" onclick="uncheckCheckAllStatusFilter()" checked=""> Expired</label></td>
                            </tr>
                            <tr>
                                <td><label style="display:block;"><input type="checkbox" name="statusfilter[]" value="Fraud" onclick="uncheckCheckAllStatusFilter()" checked=""> Fraud</label></td>
                            </tr>
                            <tr>
                                <th></th>
                            </tr>
                            </tbody></table>
                        <div class="applybtn">
                            <input type="button" value="Apply" class="btn-small btn-primary" onclick="applyStatusFilter()">
                        </div>
                    </form>
                </div>

                <form method="post" action="/billing/salabir/clientssummary.php?userid=58&amp;action=massaction">
                    <input type="hidden" name="token" value="cab00baa0b5f23abac96b5877788d3c3b008cd4d">

                    <script language="javascript">
                        $(document).ready(function(){
                            $("#prodsall").click(function () {
                                $(".checkprods").attr("checked",this.checked);
                            });
                            $("#addonsall").click(function () {
                                $(".checkaddons").attr("checked",this.checked);
                            });
                            $("#domainsall").click(function () {
                                $(".checkdomains").attr("checked",this.checked);
                            });
                        });
                    </script>

                    <table class="form" width="100%">
                        <tbody><tr><td colspan="2" class="fieldarea" style="text-align:center;"><strong>Products/Services</strong></td></tr>
                        <tr><td align="center">

                                <div class="tablebg">
                                    <table class="datatable" width="100%" cellspacing="1" cellpadding="3" border="0">
                                        <tbody><tr><th width="20"><input type="checkbox" id="prodsall"></th><th>ID</th><th>Product/Service</th><th>Amount</th><th>Billing Cycle</th><th>Signup Date</th><th>Next Due Date</th><th>Status</th><th width="20"></th></tr>
                                        <tr><td><input type="checkbox" name="selproducts[]" value="228" class="checkprods"></td><td><a href="clientsservices.php?userid=58&amp;id=228">228</a></td><td style="padding-left:5px;padding-right:5px">Web Design Package [Customized] - <a href="http://nullrefer.com/?http://rocketonline.net/" target="_blank">rocketonline.net</a></td><td> BDT. 5200.00</td><td>One Time</td><td>07/01/2019</td><td>-</td><td>Active</td><td><a href="clientsservices.php?userid=58&amp;id=228"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        <tr><td><input type="checkbox" name="selproducts[]" value="227" class="checkprods"></td><td><a href="clientsservices.php?userid=58&amp;id=227">227</a></td><td style="padding-left:5px;padding-right:5px">GIGA HOST - <a href="http://nullrefer.com/?http://rocketonline.net/" target="_blank">rocketonline.net</a></td><td> BDT. 1800.00</td><td>Annually</td><td>07/01/2019</td><td>07/01/2019</td><td>Active</td><td><a href="clientsservices.php?userid=58&amp;id=227"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        <tr><td><input type="checkbox" name="selproducts[]" value="226" class="checkprods"></td><td><a href="clientsservices.php?userid=58&amp;id=226">226</a></td><td style="padding-left:5px;padding-right:5px">GIGA HOST - <a href="http://nullrefer.com/?http://spidernetctg.com/" target="_blank">spidernetctg.com</a></td><td> BDT. 1800.00</td><td>Annually</td><td>31/12/2018</td><td>31/12/2019</td><td>Active</td><td><a href="clientsservices.php?userid=58&amp;id=226"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        <tr><td><input type="checkbox" name="selproducts[]" value="218" class="checkprods"></td><td><a href="clientsservices.php?userid=58&amp;id=218">218</a></td><td style="padding-left:5px;padding-right:5px">GIGA HOST - <a href="http://nullrefer.com/?http://amrnetbd.com/" target="_blank">amrnetbd.com</a></td><td> BDT. 1800.00</td><td>Annually</td><td>08/11/2018</td><td>08/11/2018</td><td>Active</td><td><a href="clientsservices.php?userid=58&amp;id=218"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        <tr><td><input type="checkbox" name="selproducts[]" value="184" class="checkprods"></td><td><a href="clientsservices.php?userid=58&amp;id=184">184</a></td><td style="padding-left:5px;padding-right:5px">Web Application Development [Customized] - <a href="http://nullrefer.com/?http://Online Ticket &amp; Content Management System [OTCMS]" target="_blank">Online Ticket &amp; Content Management System [OTCMS]</a></td><td> BDT. 14400.00</td><td>One Time</td><td>09/04/2018</td><td>-</td><td>Active</td><td><a href="clientsservices.php?userid=58&amp;id=184"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        <tr><td><input type="checkbox" name="selproducts[]" value="163" class="checkprods"></td><td><a href="clientsservices.php?userid=58&amp;id=163">163</a></td><td style="padding-left:5px;padding-right:5px">GIGA HOST - <a href="http://nullrefer.com/?http://orangecommunication.com.bd/" target="_blank">orangecommunication.com.bd</a></td><td> BDT. 1800.00</td><td>Annually</td><td>09/11/2017</td><td>09/11/2019</td><td>Suspended</td><td><a href="clientsservices.php?userid=58&amp;id=163"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        </tbody></table>
                                </div>

                            </td></tr></tbody></table>

                    <img src="images/spacer.gif" width="1" height="4"><br>

                    <table class="form" width="100%">
                        <tbody><tr><td colspan="2" class="fieldarea" style="text-align:center;"><strong>Addons</strong></td></tr>
                        <tr><td align="center">

                                <div class="tablebg">
                                    <table class="datatable" width="100%" cellspacing="1" cellpadding="3" border="0">
                                        <tbody><tr><th width="20"><input type="checkbox" id="addonsall"></th><th>ID</th><th>Name</th><th>Amount</th><th>Billing Cycle</th><th>Signup Date</th><th>Next Due Date</th><th>Status</th><th width="20"></th></tr>
                                        <tr><td colspan="9">No Records Found</td></tr>
                                        </tbody></table>
                                </div>

                            </td></tr></tbody></table>

                    <img src="images/spacer.gif" width="1" height="4"><br>

                    <table class="form" width="100%">
                        <tbody><tr><td colspan="2" class="fieldarea" style="text-align:center;"><strong>Domains</strong></td></tr>
                        <tr><td align="center">

                                <div class="tablebg">
                                    <table class="datatable" width="100%" cellspacing="1" cellpadding="3" border="0">
                                        <tbody><tr><th width="20"><input type="checkbox" id="domainsall"></th><th>ID</th><th>Domain</th><th>Registrar</th><th>Registration Date</th><th>Next Due Date</th><th>Expiry Date</th><th>Status</th><th width="20"></th></tr>
                                        <tr><td><input type="checkbox" name="seldomains[]" value="97" class="checkdomains"></td><td><a href="clientsdomains.php?userid=58&amp;domainid=97">97</a></td><td style="padding-left:5px;padding-right:5px"><a href="http://nullrefer.com/?http://orangecommunication.com.bd/" target="_blank">orangecommunication.com.bd</a></td><td></td><td>09/11/2017</td><td>09/11/2019</td><td>00/00/0000</td><td>Active</td><td><a href="clientsdomains.php?userid=58&amp;domainid=97"><img src="images/edit.gif" alt="Edit" width="16" height="16" border="0"></a></td></tr>
                                        </tbody></table>
                                </div>

                            </td></tr></tbody></table>

                    <img src="images/spacer.gif" width="1" height="4"><br>

                    <table class="form" width="100%">
                        <tbody><tr><td colspan="2" class="fieldarea" style="text-align:center;"><strong>Current Quotes</strong></td></tr>
                        <tr><td align="center">

                                <div class="tablebg">
                                    <table class="datatable" width="100%" cellspacing="1" cellpadding="3" border="0">
                                        <tbody><tr><th>ID</th><th>Subject</th><th>Date</th><th>Total</th><th>Valid Until Date</th><th>Status</th><th width="20"></th></tr>
                                        <tr><td colspan="7">No Records Found</td></tr>
                                        </tbody></table>
                                </div>

                            </td></tr></tbody></table>

                    <p align="center"><input type="button" value="Mass Update Items" class="button" onclick="$('#massupdatebox').slideToggle()"> <input type="submit" name="inv" value="Invoice Selected Items" class="button"> <input type="submit" name="del" value="Delete Selected Items" class="button"></p>

                    <div id="massupdatebox" style="width:75%;background-color:#f7f7f7;border:1px dashed #cccccc;padding:10px;margin-left:auto;margin-right:auto;display:none;">
                        <h2 style="text-align:center;margin:0 0 10px 0">Mass Update Items</h2>
                        <table class="form" width="100%" cellspacing="2" cellpadding="3" border="0">
                            <tbody><tr><td class="fieldlabel" width="15%" nowrap="">First Payment Amount</td><td class="fieldarea"><input type="text" size="20" name="firstpaymentamount"></td><td class="fieldlabel" width="15%" nowrap="">Recurring Amount</td><td class="fieldarea"><input type="text" size="20" name="recurringamount"></td></tr>
                            <tr><td class="fieldlabel" width="15%">Next Due Date</td><td class="fieldarea"><input type="text" size="20" name="nextduedate" class="datepick hasDatepicker" id="dp1551460144977"><img class="ui-datepicker-trigger" src="images/showcalendar.gif" alt="..." title="..."> &nbsp;&nbsp; <input type="checkbox" name="proratabill" id="proratabill"> <label for="proratabill">Create Prorata Invoice</label></td><td class="fieldlabel" width="15%">Billing Cycle</td><td class="fieldarea"><select name="billingcycle"><option value="">- No Change -</option><option value="Free Account">Free</option><option value="One Time">One Time</option><option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Semi-Annually">Semi-Annually</option><option value="Annually">Annually</option><option value="Biennially">Biennially</option><option value="Triennially">Triennially</option></select></td></tr>
                            <tr><td class="fieldlabel" width="15%">Payment Method</td><td class="fieldarea"><select name="paymentmethod"><option value="">- No Change -</option><option value="banktransfer">Bank Transfer</option><option value="bracbkash">bKash</option><option value="paypal">PayPal</option><option value="mailin">Cash Payment</option></select></td><td class="fieldlabel" width="15%">Status</td><td class="fieldarea"><select name="status"><option value="">- No Change -</option><option value="Pending">Pending</option><option value="Active">Active</option><option value="Suspended">Suspended</option><option value="Terminated">Terminated</option><option value="Cancelled">Cancelled</option><option value="Fraud">Fraud</option></select></td></tr>
                            <tr><td class="fieldlabel" width="15%">Module Commands</td><td class="fieldarea" colspan="3"><input type="submit" name="masscreate" value="Create" class="button"> <input type="submit" name="masssuspend" value="Suspend" class="button"> <input type="submit" name="massunsuspend" value="Unsuspend" class="button"> <input type="submit" name="massterminate" value="Terminate" class="button"> <input type="submit" name="masschangepackage" value="Change Package" class="button"> <input type="submit" name="masschangepw" value="Change Password" class="button"></td></tr>
                            <tr><td class="fieldlabel" width="15%">Override Auto-Suspend</td><td class="fieldarea" colspan="3"><input type="checkbox" name="overideautosuspend" id="overridesuspend"> <label for="overridesuspend">Do not suspend until</label> <input type="text" name="overidesuspenduntil" class="datepick hasDatepicker" id="dp1551460144978"><img class="ui-datepicker-trigger" src="images/showcalendar.gif" alt="..." title="..."></td></tr>
                            </tbody></table>
                        <br>
                        <div align="center"><input type="submit" name="massupdate" value="Submit"></div>
                    </div>

                </form>

            </td></tr></tbody></table>

</div>
