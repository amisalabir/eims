<?php
namespace App\ExpenseIncome;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Student extends  DB{

    private $fromTransaction,$toTransaction,$modifiedDate, $categoryid;
    private $mobile, $name, $fullname, $classid, $sectionid, $batchid,$admissiondte,$courseid,$time,$address,$groupid,$dob,$father,$mother,$fbprofile,$im,$note,$bookname;

    public function setData($postData){

        if(array_key_exists('fromTransaction',$postData)){
            $this->fromTransaction = $postData['fromTransaction'];
        }
        if(array_key_exists('toTransaction',$postData)){
            $this->toTransaction = $postData['toTransaction'];
        }
        if(array_key_exists('modifiedDate',$postData)){
            $this->modifiedDate = $postData['modifiedDate'];
            $this->admissiondte = $postData['modifiedDate'];
        }
        if(array_key_exists('studentName',$postData)){
            $this->fullname = $postData['studentName'];
        }if(array_key_exists('nickName',$postData)){
            $this->name = $postData['nickName'];
        }
        if(array_key_exists('fatherName',$postData)){
            $this->father = $postData['fatherName'];
        }
         if(array_key_exists('motherName',$postData)){
            $this->mother = $postData['motherName'];
        }
         if(array_key_exists('classid',$postData)){
            $this->classid = $postData['classid'];
        }
         if(array_key_exists('batchid',$postData)){
            $this->batchid = $postData['batchid'];
        }
        if(array_key_exists('sectionid',$postData)){
            $this->sectionid = $postData['sectionid'];
        }
        if(array_key_exists('courseid',$postData)){
            $this->courseid = $postData['courseid'];
        }if(array_key_exists('groupid',$postData)){
            $this->groupid = $postData['groupid'];
        }
        if(array_key_exists('time',$postData)){
            $this->time = $postData['time'];
        }
        if(array_key_exists('dob',$postData)){
            $this->dob = $postData['dob'];
        }
        if(array_key_exists('mobile',$postData)){
            $this->mobile = $postData['mobile'];
        }
        if(array_key_exists('nid',$postData)){
            $this->nid = $postData['nid'];
        }
        if(array_key_exists('email',$postData)){
            $this->email= $postData['email'];
        }
        if(array_key_exists('address',$postData)){
            $this->address= $postData['address'];
        }
        if(array_key_exists('remarks',$postData)){
            $this->note= $postData['remarks'];
        }
        if(array_key_exists('bookname',$postData)){
            $this->bookname= $postData['bookname'];
        }

    }
    public function store(){
  //var_dump($_POST); die();
        $arrData = array($this->mobile,$this->name,$this->fullname,$this->classid,$this->sectionid,$this->batchid,$this->admissiondte,$this->courseid,$this->time,$this->address,$this->groupid,$this->dob,$this->father,$this->mother,$this->fbprofile,$this->im,$this->note);
        $sql = "INSERT into student(`mobile`, `name`, `fullname`, `classid`, `section`, `batchid`, `admissiondate`, `courseid`, `time`, `address`, `groupid`, `dob`, `father`, `mother`, `fbprofile`, `im`, `note`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result){
                Message::message("Success! Data Has Been Inserted Successfully :)");
        }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }

    public function index(){
      // var_dump($_GET); die();
        $sql="";
        if($_GET['bookname']=='STUDENT'){
            $sql="SELECT * FROM student where student.classid='$this->classid' AND student.batchid='$this->batchid' student.soft_deleted='No'";
            $sql="SELECT student.id,student.name ,student.section, student.mobile,student.admissiondate,class.name as classname,batch.year, course.name as coursename   FROM student LEFT JOIN class on student.classid=class.id LEFT  JOIN  batch on student.batchid=batch.id LEFT  JOIN course ON student.courseid=course.id student.soft_deleted='No'";

        }
        if($_GET['bookname']=='ALLSTUDENT'){
            $sql="SELECT student.id,student.name ,student.section, student.mobile,student.admissiondate,class.name as classname,batch.year, course.name as coursename   FROM student LEFT JOIN class on student.classid=class.id LEFT  JOIN  batch on student.batchid=batch.id LEFT  JOIN course ON student.courseid=course.id WHERE student.soft_deleted='No'";

        }
        if($_GET['trashlist']=='ALLSTUDENT'){
            $sql="SELECT student.id,student.name ,student.section, student.mobile,student.admissiondate,class.name as classname,batch.year, course.name as coursename   FROM student LEFT JOIN class on student.classid=class.id LEFT  JOIN  batch on student.batchid=batch.id LEFT  JOIN course ON student.courseid=course.id WHERE student.soft_deleted='Yes'";

        }
        if($_GET['byStudent']=='on'){
            $sql="SELECT student.id,student.name , student.mobile,student.admissiondate,class.name as classname,batch.year, course.name as coursename   FROM student LEFT JOIN class on student.classid=class.id LEFT  JOIN  batch on student.batchid=batch.id LEFT  JOIN course ON student.courseid=course.id WHERE student.name  LIKE '%".$_GET['search']."%' OR class.name  LIKE '%".$_GET['search']."%' OR course.name  LIKE '%".$_GET['search']."%' OR batch.year  LIKE '%".$_GET['search']."%' OR student.mobile  LIKE '%".$_GET['search']."%' AND student.soft_deleted='No'";
        }

         $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function allClients(){

        $sql = "select * from customers where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allparticulars(){

        $sql = "select * from products where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    /*public function statement(){

        //$sql = "select * from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        $sql = "SELECT products.product_name, customers.name, salestransaction.id,salestransaction.rate,salestransaction.bandwidth,salestransaction.fromDurationDate,salestransaction.toDurationDate,salestransaction.totaldays,salestransaction.amountOut,salestransaction.amountIn,salestransaction.receiptInvoiceNo,salestransaction.particulars FROM salestransaction INNER JOIN products ON salestransaction.product_id=products.id INNER JOIN customers ON salestransaction.customerId=customers.id where salestransaction.soft_deleted='No' AND salestransaction.customerId =".$this->customer_id."  AND  salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql = "SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    } */
    public function statementTotal(){

        $sql = "select sl, sum(amount)  as totalamount  from expenseincome where soft_deleted='No' AND transactionType =".$this->transaction_Type."  AND  transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'";
        //$sql="select sl, sum(amount) as totalamount from expenseincome where soft_deleted='No' AND transactionType =1 AND transactionDate BETWEEN '2015-01-01' AND '2017-01-01'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }

    public function view(){
        $sql = "SELECT salestransaction.id,products.id as productid, products.product_name,customers.id as customerid, customers.name, salestransaction.rate,salestransaction.bandwidth,salestransaction.fromDurationDate,salestransaction.toDurationDate,salestransaction.totaldays,salestransaction.amountOut,salestransaction.amountIn,salestransaction.receiptInvoiceNo,salestransaction.particulars FROM salestransaction INNER JOIN products ON salestransaction.product_id=products.id INNER JOIN customers ON salestransaction.customerId=customers.id where salestransaction.soft_deleted='No' AND salestransaction.id =$this->id";
        //$sql = "select * from expenseincome where sl=3";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function trashed(){

        $sql="SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }
    public function update(){

        //$arrData = array($this->book_name,$this->author_name);
       // $sql = "UPDATE  expenseincome SET particulars=?,amount=? WHERE sl=".$this->sl;

        $arrData = array($this->customer_id,$this->modified_Date,$this->product_id,$this->product_rate,$this->bandwidth_quantity,$this->from_DurationDate,$this->to_DurationDate,$this->total_Date,$this->amount_Total,$this->receipt_Invoice);
        $sql = "UPDATE salestransaction SET customerId=?,modified=?,product_id=?,rate=?, bandwidth=?,fromDurationDate=?, toDurationDate=?, totaldays=?,amount=?, receiptInvoiceNo=? WHERE id=".$this->id;


        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');
    }
    public function trash(){


        $sql = "UPDATE  salesentry SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('index.php');
    }

    public function recover(){

        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");
        Utility::redirect('index.php');
    }
    public function delete(){

        $sql = "Delete from salesentry  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");

        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from salestransaction  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        }catch (PDOException $error){

            $sql = "SELECT * from salestransaction  WHERE soft_deleted = 'No'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";
        }catch (PDOException $error){

            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes'";
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  book_title SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");
        Utility::redirect('trashed.php?Page=1');
    } public function recoverMultiple($markArray){

    foreach($markArray as $id){
        $sql = "UPDATE  expenseincome SET soft_deleted='No' WHERE id=".$id;
        $result = $this->DBH->exec($sql);
        if(!$result) break;
    }

    if($result)
        Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
    else
        Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");
    Utility::redirect('index.php?Page=1');

    }

    public function deleteMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){

            $sql = "Delete from expenseincome  WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");
        Utility::redirect('index.php?Page=1');
    }

    public function listSelectedData($selectedIDs){
        foreach($selectedIDs as $id){
            $sql = "Select * from expenseincome  WHERE id=".$id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData[]  = $STH->fetch();
        }
        return $someData;
    }

    // start of search()
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND (product_name LIKE '%".$requestArray['search']."%' OR name LIKE '%".$requestArray['search']."%')GROUP BY X .id ";

        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) )

            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND product_name LIKE '%".$requestArray['search']."%' GROUP BY X .id ";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND x.receiptInvoiceNo LIKE '%".$requestArray['search']."%' GROUP BY X .id ";
        if(!isset($requestArray['byTitle']) || !isset($requestArray['byAuthor']) && isset($requestArray['id']) )

            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No' AND X.id='".$requestArray['search']."' GROUP BY X .id";

        if(isset($requestArray['advanced']))
            $sql = "SELECT X .id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.amountOut, X.amountIn, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, amountOut - amountIn bal FROM salestransaction ) X JOIN ( SELECT *, amountOut - amountIn bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X .soft_deleted = 'No'  AND (product_name LIKE '%".$requestArray['search']."%' OR X.receiptInvoiceNo LIKE '%".$requestArray['search']."%' OR name LIKE '%".$requestArray['search']."%' OR X.voucherNo LIKE '%".$requestArray['search']."%' OR X.remarks LIKE '%".$requestArray['search']."%'  ) GROUP BY X .id ";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//
    // end of search()

    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        // for each search field (particulars) block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->product_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->product_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (particulars) block end

           //for each search field () block start
        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (name)  block end


        // for each search field (receiptInvoiceNo)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receiptInvoiceNo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receiptInvoiceNo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (receiptInvoiceNo)  block end

        // for each search field (sl)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (sl)  block end

        // for each search field (PayerPayee)  block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->receivedTo);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->receivedTo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (PayerPayee)  block end

        // for each search field (transactionId)  block start
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (transactionId)  block end
        // for each search field (remarks)  block start
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->remarks);
        }
        $allData = $this->index();
        foreach ($allData as $oneData) {
            $eachString= strip_tags($oneData->remarks);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field (remarks)  block end

        return array_unique($_allKeywords);

    }// get all keywords



}