<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Classes extends  DB{
    private $transaction_Type;
    private $customer_id;
    private $branch_id;
    private $bank_id;
    private $account_head;
    private $from_Transaction;
    private $to_Transaction;
    private $transaction_Date;
    private $voucher_No;
    private $transaction_For;
    private $transaction_Mode;
    private $received_To;
    private $received_From;
    private $cheque_No;
    private $cheque_Date;
    private $remarks;
    private $amount_Total;
    private $in_Words;
    private $modified_Date;
    private $product_id;

    public function setData($postData){

        if(array_key_exists('customerId',$postData)){
            $this->customer_id = $postData['customerId']; }

        if(array_key_exists('partyid',$postData)){
            $this->customer_id = $postData['partyid']; }

        if(array_key_exists('branchid',$postData)){
            $this->branch_id = $postData['branchid']; }
        if(array_key_exists('transactionType',$postData))
        {
            $this->transaction_Type = $postData['transactionType'];
        }
        if(array_key_exists('accheadId',$postData)){
            $this->account_head = $postData['accheadId'];
        }
        if(array_key_exists('bankid',$postData)){
            $this->bank_id = $postData['bankid'];
        }
        if(array_key_exists('particulars',$postData)){
            $this->particulars_Details = $postData['particulars'];}
        if(array_key_exists('salename',$postData)){
            $this->product_id = $postData['salename'];}
        if(array_key_exists('voucherNo',$postData)){
            $this->voucher_No = $postData['voucherNo']; }
        if(array_key_exists('transactionFor',$postData)){
            $this->transaction_For = $postData['transactionFor'];}
        if(array_key_exists('transactionMode',$postData)){
            $this->transaction_Mode = $postData['transactionMode'];}
        if(array_key_exists('receivedTo',$postData)){
            $this->received_To = $postData['receivedTo'];}
        if(array_key_exists('transactionDate',$postData)){
            $this->transaction_Date = $postData['transactionDate'];}

        if(array_key_exists('fromTransaction',$postData)){
            $this->from_Transaction = $postData['fromTransaction'];
        }
        if(array_key_exists('toTransaction',$postData)){
            $this->to_Transaction = $postData['toTransaction'];
        }
        if(array_key_exists('amount',$postData)){
            $this->amount_Total = $postData['amount'];}
        if(array_key_exists('receivedFrom',$postData)){
            $this->received_From = $postData['receivedFrom'];}
        if(array_key_exists('chequeNo',$postData)){
            $this->cheque_No = $postData['chequeNo'];}
        if(array_key_exists('chequeDate',$postData)){
            $this->cheque_Date = $postData['chequeDate'];}
        if(array_key_exists('remarks',$postData)){
            $this->remarks = $postData['remarks'];}
        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];}
        if(array_key_exists('inWords',$postData)){
            $this->in_Words = $postData['inWords'];}
        $this->modified_Date=date('Y-m-d');
        if(empty($this->crvoucher_no))$this->crvoucher_no=NULL;
        if(empty($this->voucher_no))$this->voucher_no=NULL;
        if(empty($this->challan_no))$this->challan_no=NULL;
        if(empty($this->customer_id)||$this->customer_id=='0')$this->customer_id=NULL;




    }

    public function classes(){

    $sql="SELECT * FROM class WHERE soft_deleted='No'";
    $STH = $this->DBH->query($sql);
    $STH->setFetchMode(PDO::FETCH_OBJ);
    return $STH->fetchAll();

}
public function batch(){
    $sql="SELECT * FROM batch ";
    $STH = $this->DBH->query($sql);
    $STH->setFetchMode(PDO::FETCH_OBJ);
    return $STH->fetchAll();

}
    public function course(){
        $sql="SELECT * FROM course ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
}