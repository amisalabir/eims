<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Batch extends  DB{

    private $id, $headnamebangla, $headnameenglish, $position, $relatedform;

    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];
        }
        if(array_key_exists('headnamebangla',$postData)){
            $this->headnamebangla = $postData['headnamebangla'];
        }
        if(array_key_exists('headnameenglish',$postData)){
            $this->headnameenglish = $postData['headnameenglish'];
        }
         if(array_key_exists('position',$postData)){
            $this->position = $postData['position'];
        }
         if(array_key_exists('relatedform',$postData)){
            $this->relatedform = $postData['relatedform'];
        }
        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }
    }
    public function store(){

        $arrData = array($this->headnamebangla,$this->headnameenglish,$this->position,$this->relatedform,$this->modified_Date);
        $sql = "INSERT into accounthead(headnamebangla,headnameenglish,position,relatedform,created) VALUES(?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);
        if($result)
            Message::message("Success! New Head Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }
    public function book(){

        $sql="SELECT * FROM book WHERE soft_deleted='No' order by name ASC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
         return $STH->fetchAll();
    }
    public function batches(){

        $sql="SELECT * FROM batch order by year ASC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
         return $STH->fetchAll();


    }


}