<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class TrialBalance extends DB{
    private $transaction_Type,$account_head, $customer_id,$branch_id, $bank_id,$product_id;
    private $from_Transaction, $to_Transaction,$transaction_Date,$modified_Date;

    private $voucher_No;

    private $transaction_Mode,$transaction_For;
    private $received_To,$received_From;

    private $cheque_No,$cheque_Date;
    private $remarks,$in_Words;
    private $amount_Total;

    public function setData($postData){
        if(array_key_exists('customerId',$postData)){
            $this->customer_id = $postData['customerId']; }

        if(array_key_exists('partyid',$postData)){
            $this->customer_id = $postData['partyid']; }

        if(array_key_exists('branchid',$postData)){
            $this->branch_id = $postData['branchid']; }
        if(array_key_exists('transactionType',$postData))
        {
            $this->transaction_Type = $postData['transactionType'];
        }
        if(array_key_exists('accheadId',$postData)){
            $this->account_head = $postData['accheadId'];
        }
        if(array_key_exists('bankid',$postData)){
            $this->bank_id = $postData['bankid'];
        }
        if(array_key_exists('particulars',$postData)){
            $this->particulars_Details = $postData['particulars'];}
        if(array_key_exists('salename',$postData)){
            $this->product_id = $postData['salename'];}
        if(array_key_exists('voucherNo',$postData)){
            $this->voucher_No = $postData['voucherNo']; }
        if(array_key_exists('transactionFor',$postData)){
            $this->transaction_For = $postData['transactionFor'];}
        if(array_key_exists('transactionMode',$postData)){
            $this->transaction_Mode = $postData['transactionMode'];}
        if(array_key_exists('receivedTo',$postData)){
            $this->received_To = $postData['receivedTo'];}
        if(array_key_exists('transactionDate',$postData)){
            $this->transaction_Date = $postData['transactionDate'];}

        if(array_key_exists('fromTransaction',$postData)){
            $this->from_Transaction = $postData['fromTransaction'];
        }
        if(array_key_exists('toTransaction',$postData)){
            $this->to_Transaction = $postData['toTransaction'];
        }
        if(array_key_exists('amount',$postData)){
            $this->amount_Total = $postData['amount'];}
        if(array_key_exists('receivedFrom',$postData)){
            $this->received_From = $postData['receivedFrom'];}
        if(array_key_exists('chequeNo',$postData)){
            $this->cheque_No = $postData['chequeNo'];}
        if(array_key_exists('chequeDate',$postData)){
            $this->cheque_Date = $postData['chequeDate'];}
        if(array_key_exists('remarks',$postData)){
            $this->remarks = $postData['remarks'];}
        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];}
        if(array_key_exists('inWords',$postData)){
            $this->in_Words = $postData['inWords'];}
        $this->modified_Date=date('Y-m-d');
        if(empty($this->crvoucher_no))$this->crvoucher_no=NULL;
        if(empty($this->voucher_no))$this->voucher_no=NULL;
        if(empty($this->challan_no))$this->challan_no=NULL;
        if(empty($this->customer_id)||$this->customer_id=='0')$this->customer_id=NULL;
    }

    #################### Trial Balance #####################
    public function bankTrialBalance(){
        $sql="";
        if($this->branch_id=='all'){$sql = "SELECT X .id, bank.bankname, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountOut)-SUM(X.amountIn) AS balance,  X.bankid,  X.remarks FROM salestransaction X LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59' GROUP BY bank.id WITH ROLLUP;";}
        if($this->branch_id!='all'){$sql = "SELECT X .id, bank.bankname, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountOut)-SUM(X.amountIn) AS balance,  X.bankid,  X.remarks FROM salestransaction X LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59'  AND X.branchid='$this->branch_id' GROUP BY bank.id WITH ROLLUP;";}

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function partyTrialBalance(){
        $sql="";
        if($this->branch_id=='all'){$sql = "SELECT X .id, party.partyname,  SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountIn)-SUM(X.amountOut) AS balance,   X.remarks FROM salestransaction X LEFT JOIN party ON X.customerId = party.id LEFT JOIN sales ON X.challanno = sales.challanno  WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='0' AND X.customerId !=0 GROUP BY X.customerId WITH ROLLUP;";}
        if($this->branch_id!='all'){$sql = "SELECT X .id, party.partyname,  SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountIn)-SUM(X.amountOut) AS balance,   X.remarks FROM salestransaction X LEFT JOIN party ON X.customerId = party.id LEFT JOIN sales ON X.challanno = sales.challanno  WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='0' AND X.customerId !=0 AND X.branchid='$this->branch_id'GROUP BY X.customerId WITH ROLLUP;";}
           $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function saleTrialBalance(){
        $sql = "";
        if($this->branch_id=='all'){$sql = "SELECT X .id,products.product_name, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountOut)-SUM(X.amountIn) AS balance,  X.bankid,  X.remarks FROM salestransaction X LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='24' GROUP BY X.product_id WITH ROLLUP;";}
        if($this->branch_id!='all'){$sql = "SELECT X .id,products.product_name, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountOut)-SUM(X.amountIn) AS balance,  X.bankid,  X.remarks FROM salestransaction X LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='24' AND X.branchid='$this->branch_id' GROUP BY X.product_id WITH ROLLUP;";}

$STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function headTotal(){
        $sql = "";
        if($this->branch_id=='all'){$sql = "SELECT accounthead.headnameenglish, sum(salestransaction.amountOut) as debit,SUM(salestransaction.amountIn)as credit, SUM(salestransaction.amountIn)-SUM(salestransaction.amountOut) as balance FROM salestransaction LEFT JOIN accounthead on salestransaction.accountheadid=accounthead.id WHERE salestransaction.accountheadid='$this->account_head' AND salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND salestransaction.soft_deleted='No' ";}
        if($this->branch_id!='all'){$sql = "SELECT accounthead.headnameenglish, sum(salestransaction.amountOut) as debit,SUM(salestransaction.amountIn)as credit , SUM(salestransaction.amountIn)-SUM(salestransaction.amountOut) as balance FROM salestransaction LEFT JOIN accounthead on salestransaction.accountheadid=accounthead.id WHERE salestransaction.accountheadid='$this->account_head' AND branchid='$this->branch_id' AND salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND salestransaction.soft_deleted='No' ";}
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function headWiseStatement(){
        ///echo $this->branch_id; echo $this->account_head;

        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,accounthead.headnamebangla,bank.accountname, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.accountheadid='$this->account_head' AND  X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    #################### Trial Balance ended ##############
    #################### Cash in Hand #####################
    public function cashInHandHeadOffice(){
        //var_dump($_GET);
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='1' AND X.transactionType <> 'OB'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        //return $STH->fetchAll();

        $totalCashIn=0;
        $totalCashOut=0;
        $CashBalance=0;
        foreach($STH->fetchAll() as $oneData){

            $totalCashIn=$totalCashIn+$oneData->amountIn;
            $totalCashOut=$totalCashOut+$oneData->amountOut;
            $CashBalance=($CashBalance-$oneData->amountOut)+$oneData->amountIn;

        }
        return $CashBalance;

    }
    public function cashInHandYard(){
        //var_dump($_GET);
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='2' AND X.transactionType <> 'OB'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        //return $STH->fetchAll();
        $totalCashIn=0;
        $totalCashOut=0;
        $CashBalance=0;
        foreach($STH->fetchAll() as $oneData){

            $totalCashIn=$totalCashIn+$oneData->amountIn;
            $totalCashOut=$totalCashOut+$oneData->amountOut;
            $CashBalance=($CashBalance-$oneData->amountOut)+$oneData->amountIn;

        }
        return $CashBalance;

    }
    public function cashInHandPetty(){
        //var_dump($_GET);
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='3' AND X.transactionType <> 'OB'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        //return $STH->fetchAll();

        $totalCashIn=0;
        $totalCashOut=0;
        $CashBalance=0;
        foreach($STH->fetchAll() as $oneData){

            $totalCashIn=$totalCashIn+$oneData->amountIn;
            $totalCashOut=$totalCashOut+$oneData->amountOut;
            $CashBalance=($CashBalance-$oneData->amountOut)+$oneData->amountIn;

        }
        return $CashBalance;

    }
    #################### Cash in Hand #####################




}