<?php
namespace App\ExpenseIncome;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Search extends DB {

     public function index(){

               // $sql="SELECT * FROM transactionentries WHERE `soft_deleted` ='No' ";
                $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM transactionentries X LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No'  AND X.transactionType <> 'OB'";

                $STH = $this->DBH->query($sql);
                $STH->setFetchMode(PDO::FETCH_OBJ);
                return $STH->fetchAll();
    }

    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from transactionentries  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        }catch (PDOException $error){

            $sql = "SELECT * from transactionentries  WHERE soft_deleted = 'No'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";
        }catch (PDOException $error){

            $sql = "SELECT * from expenseincome  WHERE soft_deleted = 'Yes'";
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }

    public function search($requestArray){

        $sql = "";
        $sql = "SELECT * FROM `transactionentries` WHERE `soft_deleted` ='No' AND `remarks` LIKE '%".$requestArray['search']."%'";

        if(isset($requestArray['byRemarks']) && isset($requestArray['byHead']) )
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM transactionentries X LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No'  AND X.transactionType <> 'OB' AND X.remarks LIKE '%".$requestArray['search']."%' OR student.name LIKE '%".$requestArray['search']."%' OR accounthead.headnamebangla LIKE '%".$requestArray['search']."%'";

        if(isset($requestArray['byRemarks']) && !isset($requestArray['byHead']) )
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM transactionentries X LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No'  AND X.transactionType <> 'OB' AND X.remarks LIKE '%".$requestArray['search']."%' OR student.name LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byRemarks']) && isset($requestArray['byHead']) )
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM transactionentries X LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No'  AND X.transactionType <> 'OB' AND student.name LIKE '%".$requestArray['search']."%' OR accounthead.headnamebangla LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['Student']))
        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM transactionentries X LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No'  AND X.transactionType <> 'OB' AND student.name LIKE '%".$requestArray['search']."%' OR accounthead.headnamebangla LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();
        return $someData;

    }// end of search()


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

       $allData = $this->index();


        // for each search field block for remark
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->remarks);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }

         // for each search field block for studentname
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->studentname);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end

        return array_unique($_allKeywords);


    }// get all keywords


}