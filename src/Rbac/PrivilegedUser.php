<?php

class PrivilegedUser EXTENDS Role{
    private  $roles;

    public function __construct() {
        parent::__construct();
        $this->roles = array();
    }
    public function objectToarray($object){
        return $myArray = json_decode(json_encode($object), true);
    }

    // override User method
    public  function getByUsername($username) {
        
        $sql = "SELECT * FROM users WHERE username='$username'";      
        $STH =$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);      
        $result = $STH->fetchAll();
     
        
        if (!empty($result)) {
            $privUser = new PrivilegedUser();
            
            $myresult=$this->objectToarray($result);

            $privUser->user_id = $myresult[0]['user_id'];
            $privUser->username = $myresult[0]["username"];
            $privUser->password = $myresult[0]["password"];
            $privUser->email_addr = $myresult[0]["email"];
            $privUser->initRoles();
            //var_dump($privUser);
            return $privUser;

        } else {
            return false;
        }
        
       
    }

    // populate roles with their associated permissions
    protected function initRoles(){
       // $this->roles = array();
        $sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1
                JOIN roles as t2 ON t1.role_id = t2.role_id
                WHERE t1.user_id = '$this->user_id'";
        $STH =$this->DBH->query($sql);

/*
        $row = $STH->fetch(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
          //echo $value;
          $this->roles[$row["role_name"]] = Role::getRolePerms($row["role_id"]);
        }
        */
        while($row = $STH->fetch(PDO::FETCH_ASSOC)){
          
            $this->roles[$row["role_name"]] = Role::getRolePerms($row["role_id"]);
            //var_dump($this->roles[$row["role_name"]]);
        }
            
      //var_dump($this->roles);
    }

    // check if user has a specific privilege
    public function hasPrivilege($perm) {
     //var_dump($this->roles);
        foreach ($this->roles as $role) {
            if ($role->hasPerm($perm)) {
                return true;
            }
        }
        echo "Access Denied !";
        return false;
    }

// check if a user has a specific role
public function hasRole($role_name) {

    if(isset($this->roles[$role_name])){
        return TRUE;
    } echo "<br>Access Denied !";
    
}

// insert a new role permission association
public static function insertPerm($role_id, $perm_id) {
    $sql = "INSERT INTO role_perm (role_id, perm_id) VALUES (:role_id, :perm_id)";
    $sth = $GLOBALS["DB"]->prepare($sql);
    return $sth->execute(array(":role_id" => $role_id, ":perm_id" => $perm_id));
}

// delete ALL role permissions
public static function deletePerms() {
    $sql = "TRUNCATE role_perm";
    $sth = $GLOBALS["DB"]->prepare($sql);
    return $sth->execute();
}
    
}